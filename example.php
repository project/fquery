<?php

// Bootstrap drupal
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

if (!module_exists('fquery')) {
  die ('fQuery module must be installed and enabled.');
}

// Fetch and prepare the "add story" form.
// Note: this is NOT proper use, this is just for demonstration purposes.
// Several properies added by drupal_prepare_form() will be missing. They are not relevant for these example queries.
$form = drupal_retrieve_form('story_node_form');
$form = form_builder('story_node_form', $form);
$form['author']['#attributes'] = array('class' => 'foo bar');

// Try these queries
$queries = array(
  'textfield',
  'textfield:not([#title=Authored on])',
  'fieldset[class=foo bar]',
  '[#type=fieldset] > textfield:last-child',
  'fieldset + fieldset',
  'checkbox#sticky, checkbox#status, checkbox#promote',
  'textarea',
);

foreach ($queries as $i => $query) {

  print '<h1>Query: '. check_plain($query) .'</h1>';

  // Do query
  timer_start($timer = 'fquery'. $i);
  $query = f($query, $form);
  print '<h4>Time taken: '. timer_read($timer) .'</h4>';

  // Render result set
  foreach ($query as $i => &$element) {
    // Make sure we can render the same element multiple times if needed.
    $copy = unserialize(serialize($element));

    print '<h2>Element '. ($i + 1) .'</h2>';
    print drupal_render_form('', $copy);
    print("\n\n");
  }
  print '<hr />';    
}

